from setuptools import setup

description = """Export a Conda environment with --from-history, but also append
Pip-installed dependencies

Exports only manually-installed dependencies, excluding build versions, but
including Pip-installed dependencies.

Lots of issues requesting this functionality in the Conda issue tracker, no
sign of progress (as of March 2020).

Usage: from inside an active Conda environment, run the 'conda-env-export' command.

TODO (?): support command-line flags -n and -p
"""

setup(
    name='conda-env-export',
    author='Greg Werbin',
    author_email='gregory.werbin@chubb.com',
    version='0.1.5',
    long_description=description,
    long_description_content_type='text/plain',
    package_dir={'': 'src'},
    py_modules=['conda_env_export'],
    install_requires=['pyyaml'],
    entry_points={
        'console_scripts': [
            'conda-env-export = conda_env_export:main'
        ]
    }
)

