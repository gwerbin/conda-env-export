build:
	python setup.py sdist bdist_wheel

clean:
	rm -rf build/
	rm -rf dist/

.PHONY: build release

