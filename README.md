Export a Conda environment with --from-history, but also append
Pip-installed dependencies

Exports only manually-installed dependencies, excluding build versions, but
including Pip-installed dependencies.

Lots of issues requesting this functionality in the Conda issue tracker, no
sign of progress (as of March 2020).

TODO (?): support command-line flags -n and -p

# Installation

```bash
pip install 'git+https://github.aceins.com/Gregory-Werbin/conda-env-export#egg=conda-env-export'
```

# Usage

In your current environment:

```bash
conda-env-export > environment.yml
```

Then create an environment with:

```bash
conda env create -f environment.yml
```

## Example

Create a Conda environment, then export and create a 2nd environment based on the first:

```bash
conda create -p ./env1 python pandas twine
conda activate ./env1
pip install sentencepiece
pip install 'git+https://github.aceins.com/Gregory-Werbin/conda-env-export#egg=conda-env-export'
conda-env-export > environment.yml
conda env create -p ./env2 -f environment.yml
```

Example output, from the above environment:

```yaml
channels:
- defaults
- conda-forge
dependencies:
- pandas=1.0.3
- python=3.8.2
- twine=2.0.0
- pip:
  - conda-env-export==0.1
  - pyyaml==5.3.1
  - sentencepiece==0.1.85
```

